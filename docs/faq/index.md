# FAQ
<details open>
<summary> <h2>1. How to Connect to GEM DCS </h2> </summary>

First use Remote Desktop applications to establish a remote connection to ```cerntsnew``` with your CERN user credentials. Next, similarly, connect remotely to ```cerntscms``` with CERN user credentials, select ```GEM DCS```.   
:high_brightness: If you do not have access to *GEM DCS* on the cerntscms machine, please contact GEM RC Team as indicated in the [Getting Started](https://cmsgemonline.web.cern.ch/doc/latest/contact/) page.  
</details>

<details open>
<summary> <h2>2. In General, How to Power On HV of a Chamber? </h2></summary>

- The prerequisite of this operation is the status of the chamber is either **OFF** or **ERROR**. if it's in the ERROR state, first ```Clear Alarm```.   
- First ```Take Local Control``` at the FSM Control interface. Ensure that you establish the necessary communication with the Technical Shifter, you can only click "Take Local Control" after TS releases the central DCS control. 
- At *FSM*, locate the endcap of the chamber, i.e. ```GEM_ENDCAP_Plus``` or ```GEM_ENDCAP_Minus```, and double-click on it.
- In the Sub-System column, find the chamber's name and click on the drop-down menu labeled "State". First select the command ```GO_TO_STAMDBY```, initiate to the chamber.
- Wait until the chamber reaches a stable **STANDBY** status. You will know when the State block turns blue.
- Afterwards, again click on the State drop-down menu, select the command ```SWITCH_ON_HV``` and initiate to the chamber. Wait until the chamber reaches a stable HV state (in green).
</details>

<details open>
<summary> <h2>3. How to Power On HV of a Chamber Manually? </h2></summary>

</details>

<details open>
<summary> <h2>4. What Happens When There is a Trip on a Chamber? </h2></summary>

</details>

<details open>
<summary> <h2>5. What Happens When There is a Short on a Chamber? </h2></summary>

</details>

<details open>
<summary> <h2>6. How to Check Trend Plots? </h2></summary>

</details>

<details open>
<summary> <h2>3. How to Set HV Recipt? </h2></summary>

</details>