# Operations 

Hello! Congratulations, you're taking one of the first steps to
becoming an expert on the GEM DAQ electronics. For you to get the most
out of this guide there's a couple of things that we should discuss
first. The first is how this guide should be used. It's broken down
into several sections, each section focuses on a specific topic. Linked
here you will find various information related to the GEM online
software and DAQ operations.

Now go ahead and choose the topic you were looking for information on,
or use the search feature.
