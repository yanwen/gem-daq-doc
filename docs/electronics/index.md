# Electronics primer

# Back-end electronics

The GEM back-end electronics system is composed of custom μTCA (and
ATCA) FPGA processor cards. The processor cards are responsible for
slow-control, translating trigger and timing signals coming from the
central system, building events, sending the events to the central DAQ
system, and sending trigger stubs to the EMTF for inclusion in the muon
trigger system of CMS. Information is given in the linked documents on
interacting with the different components, as well as how to deal with
pre and post power cut situations

## Front-end electronics

Front-end electronics are those present on the detector itself. The
primary component is the `VFAT3<gemos-frontend-vfat3>`{.interpreted-text
role="ref"} chip, which translates the detector readout signals into hit
information. The
`OptoHybrid<gemos-frontend-optohybrid>`{.interpreted-text role="ref"}
links the back-end electronics with the detector via custom designed,
radiation hard, optical links (VTTx/VTRx) and custom radiation hard
ASICs (`GBTx<gemos-frontend-gbtx>`{.interpreted-text role="ref"} and
`SCA<gemos-frontend-sca>`{.interpreted-text role="ref"}). Power is
distributed to the on-detector components via a custom DC-DC converter,
the `FEASTMP<gemos-frontend-feast>`{.interpreted-text role="ref"}. The
documents linked below will provide you with a brief overview of the
system so that you can operate. For more detailed information, you will
find more details in the
`corresponding seciton in the expert guide<expertguide:gemos-frontend-guide>`{.interpreted-text
role="ref"}

### Low voltage powering

Two LV settings are recommended depending on what equipment you have
available.

!!! note
    Because the voltage at the power supply will *not* be the voltage at the
    terminals, especially if your cable is long. It is recommended to use a
    voltage drop compensating power supply with sense wires at the LV cable
    connector to the detector patch panel.

-   If your power supply can go beyond 8V (e.g., A3016HP), then provide
    8V at the LV terminals of the detector. Here the system should draw
    \~3A when fully configured (VFATs in sleep mode) and increase to
    \~3.5A when VFATs are in run mode.

-   If your power supply cannot supply higher than 8V (e.g., A3016) then
    provide 6.5V at the LV terminals of the detector. Here the system
    will draw closer to \~5.5A when fully configured (VFATs in sleep
    mode) and move to \~6A when VFATs are placed in run mode.

    !!! Warning
        At this operating point the FEASTs will be much less efficient and
        may overheat easily. Normally this happens to the F1 and/or F2
        FEASTs, which respectively supply the FPGA core voltage or VTRx/VTTx
        power. It is strongly recommended to have a fan blowing cool air
        over these FEASTs, and heat sinks on *all* FEASTs.
