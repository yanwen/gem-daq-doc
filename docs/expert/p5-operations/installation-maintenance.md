# P5 installation and maintenance

Even more than the other pages in the expert guide, this page is meant for experts and administrators.
Failing to follow the instructions or failing to react to unexpected events can result in leaving the GEM sub-system in a non-working state.

**Please always post an e-log in the `GEM > P5 > DAQ` sub-section with the modifications you implemented.**

!!! note
    * Only the members of the `cms_gem_librarian` group are allowed to use the `dropbox2` mechanism to update RPMs.
    * Modifications applied to the backend boards can be carried on by any user with the knowledge of the passwords and enough experience.
      For convenience, members of the `gempro` group can login as the `gempro` user and connect to any board without any password.
    * System modifications to the GEM machines can (but should not) be carried on by members of the `gemsudoers` group.
    * Traditionally, the `gempro` and `gemsudoers` groups are kept in sync' and meant for DAQ experts; the `cms_gem_librarian` is a sub-group meant for administrators and superusers.

!!! warning
    All CTP7 volumes are kept in read-only mode during normal operations in order to avoid data corruption in case of a sudden power cut.
    During backend boards maintenance, the `/mnt/persistent` partition must be remounted in read-write mode:
    ``` sh
    # from gempro@gemvm-control
    mussh -m 13 -H /gemdata/config/ctp7-ge* -l root -c 'mount -o remount,rw /mnt/persistent'
    ```
    Do no forget to put back the partition in read-only mode after maintenance:
    ``` sh
    # from gempro@gemvm-control
    mussh -m 13 -H /gemdata/config/ctp7-ge* -l root -c 'mount -o remount,ro /mnt/persistent'
    ```
    Such an operation is taken care of automatically in the `push-configuration.sh` script & co.

## Online software

### Client software

Updates can be installed with RPMs through the centrally provided `dropbox2` mechanism:

``` sh
# from cmsdropbox
sudo dropbox2 -s gem -u <path-to-the-rpm-folder>
```

!!! warning
    Some packages may depend on another. Please update all
    dependent packages at once to avoid installation issues.
    E.g. always install the same version of the `cmsgemos`
    and `cmsgemos-local-readout` packages.

### Backend software

At the moment, the backend online software must be manually installed from builds, either manual or from the CI.
It is expected to include the artifacts into the RPM in order to simplify the deployment and avoid synchronization mistakes.

``` sh
# from gempro@gemvm-control

wget -e https_proxy=cmsproxy:3128 https://cmsgemos.web.cern.ch/repos/builds/cmsgemos-ctp7-ge11-<version>.tar.xz -O - | tar -xvJ -C /gemdata/software/ctp7-ge11/
wget -e https_proxy=cmsproxy:3128 https://cmsgemos.web.cern.ch/repos/builds/cmsgemos-ctp7-ge21-<version>.tar.xz -O - | tar -xvJ -C /gemdata/software/ctp7-ge21/

for host in $(cat /gemdata/config/ctp7-ge11.txt); do rsync -v -a --delete --include='/bin/***' --include='/lib/***' --exclude='*' /gemdata/software/ctp7-ge11/mnt/persistent/gempro/ gempro@${host}:; done
for host in $(cat /gemdata/config/ctp7-ge21.txt); do rsync -v -a --delete --include='/bin/***' --include='/lib/***' --exclude='*' /gemdata/software/ctp7-ge21/mnt/persistent/gempro/ gempro@${host}:; done
```

!!! tip
    Do not hesitate to empty the `ctp7-ge11` and `ctp7-ge21` folders regularly.
    This prevents the installation of outdated files to the backend boards.

## Firmware

All tested firmware are published into an EOS area and should be synchronized with the NAS area whenever needed.

``` sh
# from gempro@gemvm-control
rsync -v -a --delete -e "ssh -J cmsusr" <username>@lxplus:/eos/project-c/cmsgemonline/www/cmsgemos/repos/firmware/ /gemdata/firmware/
```

### CTP7 update

The CTP7 firmware update is done in two steps: first, the files synchronization to the backend boards memory; second, the symlinks update on each of the backend boards.
Use the following commands as needed.

``` sh
# from gempro@gemvm-control

# files synchronization
for host in $(cat /gemdata/config/ctp7-ge*); do rsync -v -a --delete --delete-excluded --include='/ctp7-*/***' --include='/oh-*/***' --exclude='*' /gemdata/firmware/ gempro@${host}:share/0xbefe/; done

# backend firmware update
mussh -m 12 -H /gemdata/config/ctp7-ge11.txt -l gempro -c 'bash -l -c "ln -sfnv ../../share/0xbefe/<ctp7-release> etc/cmsgemos/backend-firmware"'
mussh -m 1 -H /gemdata/config/ctp7-ge21.txt -l gempro -c 'bash -l -c "ln -sfnv ../../share/0xbefe/<ctp7-release> etc/cmsgemos/backend-firmware"'

# OptoHybrid firmware update
mussh -m 12 -H /gemdata/config/ctp7-ge11.txt -l gempro -c 'bash -l -c "ln -sfnv ../../share/0xbefe/<oh-release> etc/cmsgemos/optohybrid-firmware"'
mussh -m 1 -H /gemdata/config/ctp7-ge21.txt -l gempro -c 'bash -l -c "ln -sfnv ../../share/0xbefe/<oh-release> etc/cmsgemos/optohybrid-firmware"'
```

!!! tip
    Do not forget to update the address table!
    See the [tips section](#tips).

### AMC13 update

Use the required `AMC13Tool2.exe` tool commands, once per AMC13 in the system:

* Use the `pk` command to program the Kintex-7
* Use the `ps` command to program the Spartan-6

The `AMC13Tool2.exe` can be launched as follow:
``` sh
# from gempro@gemvm-control

cd /gemdata/firmware/<build>
LD_LIBRARY_PATH=/opt/cactus/lib /opt/cactus/bin/amc13/AMC13Tool2.exe -c /gemdata/config/amc13.xml -i amc-s2e01-04-13
LD_LIBRARY_PATH=/opt/cactus/lib /opt/cactus/bin/amc13/AMC13Tool2.exe -c /gemdata/config/amc13.xml -i amc-s2e01-14-13
LD_LIBRARY_PATH=/opt/cactus/lib /opt/cactus/bin/amc13/AMC13Tool2.exe -c /gemdata/config/amc13.xml -i amc-s2e01-23-13
```

## Tips

* The LMDB address table can be updated on all backend boards with the following command.
  It should ideally be repeated after each firmware or backend software update.
  ``` sh
  # from gempro@gemvm-control
  mussh -m 13 -H /gemdata/config/ctp7-ge*.txt -l gempro -c 'bash -l -c "gem-update-address-table"'
  ```

* Check the connectivity with all backend boards via:
  ``` sh
  # from gempro@gemvm-control
  for host in $(cat /gemdata/config/ctp7-ge*); do ping -c1 -W1 ${host}; done
  ```

* The backend board services can be recovered with:
  ``` sh
  # from gempro@gemvm-control
  mussh -m 13 -H /gemdata/config/ctp7-ge* -l root -c 'bash -l -c "source /mnt/persistent/gempro/.profile; killall -9 memhub-server; nohup memhub-server /dev/mem >/dev/null 2>&1 & disown"'
  mussh -m 13 -H /gemdata/config/ctp7-ge* -l gempro -c 'bash -l -c "killall -9 gemrpc; bin/gemrpc"'
  ```
  **Note that recovering twice the `gemrpc` server may be required if connections were open.**
