# CMS GEM DAQ Documentation

Prepare your environment:

``` bash
python3 -m venv pyvenv
pyvenv/bin/pip install --upgrade pip
pyvenv/bin/pip install -r requirements.txt
```

Start a local web server to visualize your changes:

``` bash
pyvenv/bin/mkdocs serve
```
